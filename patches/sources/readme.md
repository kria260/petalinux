# PetaLinux Project Description
[Device-Tree-Binary](https://docs.xilinx.com/r/2021.1-English/ug1144-petalinux-tools-reference-guide/PetaLinux-Project-Structure)

```
/project-spec/meta-user/recipes-bsp/device-tree/files/	
```
system-user.dtsi is not modified by any PetaLinux tools. This file is safe to use with revision control systems. In addition, you can add your own DTSI files to this directory. You have to edit the <plnx-proj-root>/project-spec/meta-user/recipes-bsp/device-tree/device-tree.bbappend by adding your DTSI file.


# Setting up Device Tree Binary
[Device-Tree-Binary](https://xilinx-wiki.atlassian.net/wiki/spaces/A/pages/2423488521/OpenAMP+Project+Build+Process#Setting-up-Device-Tree-Binary)

The PetaLinux reference BSP includes a DTB (Device Tree Binary) for OpenAMP located at:
```
<plnx-user-proj-root>/pre-built/linux/images/openamp.dtb 
```
The device tree setting for the shared memory and the kernel remoteproc is demonstrated in:
```
<plnx-user-proj-root>/project-spec/meta-user/recipes-bsp/device-tree/files/openamp.dtsi
```
The openamp.dtb and openamp.dtsi files are provided for reference only. You can refer to the contents from these files to edit the system-user.dtsi file to include the content from openamp.dtsi for your project. Either you can copy paste the contents of openamp.dtsi in the system-user.dtsi or you can update system-user.dtsi to include openamp.dtsi (or any other reference dtsi file) as follows:

Append below line in system-user.dtsi:
```
/include/ "openamp.dtsi"  or  /include/ "<your-updated-dtsi-filename>.dtsi"
```
If you do above “include file step” then you will also have to give reference to this file to append device tree recipe as follows:

bbappend file to be updated can be found at:
```
<plnx-user-proj-root>/project-spec/meta-user/recipes-bsp/device-tree/device-tree.bbappend
```
Update below line to include openamp.dtsi
```
SRC_URI:append = " file://config file://system-user.dtsi"
```
to
```
SRC_URI:append = " file://config file://system-user.dtsi file://openamp.dtsi"
```
```
diff -u file_old.dtsi file_new.dtsi > file.patch
```
To analice device tree use

```
dtc -I dtb -O dts *.dtb > dt.txt
```

