set project_name [lindex $argv 0]

open_project tmp/$project_name.xpr

#write_hw_platform -fixed -include_bit -force -file  tmp/$project_name.xsa
write_hw_platform -hw -include_bit -force -file tmp/$project_name.xsa

close_project
