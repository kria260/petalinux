set project_name [lindex $argv 0]

set part_name [lindex $argv 1]

open_project tmp/$project_name.xpr

set my_pwd [pwd]
puts "get_property DIRECTORY [current_run] : [pwd]"

#VHDL TOOL
set_property target_language VHDL [current_project]

update_ip_catalog

set bd_path tmp/$project_name.srcs/sources_1/bd/design_1 
# design_1 by system

#create_bd_design -force design_1

if {[version -short] >= 2016.3} {
  set_property synth_checkpoint_mode None [get_files $bd_path/design_1.bd]
}

generate_target all [get_files $bd_path/design_1.bd]
make_wrapper -files [get_files $bd_path/design_1.bd] -top

#add_files -norecurse $bd_path/hdl/system_wrapper.v
add_files -norecurse $bd_path/hdl/design_1_wrapper.vhd

set_property TOP design_1_wrapper [current_fileset]

#set files [glob -nocomplain projects/$project_name/*.v projects/$project_name/*.sv]
set files [glob -nocomplain projects/$project_name/*.vhd]
if {[llength $files] > 0} {
  add_files -norecurse $files
}

foreach f $files {
  set_property IS_GLOBAL_INCLUDE TRUE [get_files $f]
}

#remove old XDC files
remove_files -fileset constrs_1 Kria_K26_SOM_Rev1.xdc ports.xdc -quiet

set files [glob -nocomplain $my_pwd/cfg/*.xdc $my_pwd/projects/*.xdc]
if {[llength $files] > 0} {
  add_files -norecurse -fileset constrs_1 $files
}

set files [glob -nocomplain $my_pwd/cfg/*.xdc $my_pwd/projects/*.xdc]
if {[llength $files] > 0} {
  add_files -norecurse -fileset constrs_1 $files
}

#set_property VERILOG_DEFINE {TOOL_VIVADO} [current_fileset]

set_property STRATEGY Flow_PerfOptimized_high [get_runs synth_1]
set_property STRATEGY Performance_NetDelay_high [get_runs impl_1]



close_project
