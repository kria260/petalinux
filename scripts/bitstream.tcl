set project_name [lindex $argv 0]

open_project tmp/$project_name.xpr

if {[get_property PROGRESS [get_runs impl_1]] != "100%"} {
  launch_runs impl_1 -to_step route_design
  wait_on_run impl_1
}

set filename_wrapper [find_top]

open_run [get_runs impl_1]

set_property BITSTREAM.GENERAL.COMPRESS true [current_design]
set_property STEPS.WRITE_BITSTREAM.ARGS.BIN_FILE true [get_runs impl_1]

write_bitstream -force -bin_file -file tmp/$project_name.runs/impl_1/$filename_wrapper.bit

close_project
