LD_LIBRARY_PATH =

NAME = test
PART = xck26-sfvc784-2LV-c
PROC = psu_cortexa53_0
BOARD = k26som
CORES =

VIVADO = vivado -nolog -nojournal -mode batch
XSCT = xsct
RM = rm -rf

UBOOT_TAG = 2022.2
LINUX_TAG = 5.10
DTREE_TAG = xlnx_rel_v2022.2
KR260_TAG = v2022.2-10141622
K26SOM_TAG = v2022.2-10141622

ROOT_DIR=$(realpath $(dir $(lastword $(MAKEFILE_LIST))))
ABS_BUILD_PATH=$(ROOT_DIR)/tmp
PETALINUX_PROJ_NAME=petalinux
ABS_SW_PATH=$(ABS_BUILD_PATH)/$(PETALINUX_PROJ_NAME)
PETALINUX_DIR=$(ABS_SW_PATH)
PETALINUX_CONFIG=$(PETALINUX_DIR)/project-spec/configs/config
PETALINUX_ROOTFS_CONFIG=$(PETALINUX_DIR)/project-spec/configs/rootfs_config

XSA_PATH 	?=${PLATFORM_REPO_PATHS}/xilinx_zcu104_base_202220_1/hw/
XSA_NAME	?=hw

IMAGE_DIR=$(ABS_BUILD_PATH)/$(PETALINUX_PROJ_NAME)/pre-built/linux/images
SD_CARD =$(ABS_BUILD_PATH)/sdcard

UBOOT_URL = https://ftp.denx.de/pub/u-boot/u-boot-$(UBOOT_TAG).tar.bz2
LINUX_URL = https://cdn.kernel.org/pub/linux/kernel/v5.x/linux-$(LINUX_TAG).46.tar.xz
DTREE_URL = https://github.com/Xilinx/device-tree-xlnx/archive/$(DTREE_TAG).tar.gz

DTREE_DIR = $(ABS_BUILD_PATH)/dt/dt
DT_PATH  = $(DTREE_DIR)/$(BOARD)/psu_cortexa53_0/device_tree_domain/bsp

KR260_BSP = xilinx-kr260-starterkit-$(KR260_TAG).bsp
K26SOM_BSP = xilinx-k26-som-$(K26SOM_TAG).bsp

KR260_BSP_URL = https://www.xilinx.com/member/forms/download/xef.html?filename=xilinx-kr260-starterkit-$(KR260_TAG).bsp
K26SOM_BSP_URL = https://www.xilinx.com/member/forms/download/xef.html?filename=xilinx-k26-som-$(K26SOM_TAG).bsp

CURRENT_BSP = $(K26SOM_BSP) 
CURRENT_BSP_URL = $(KR260_BSP_URL) 

ifeq ($(BOARD), k26som)
 CURRENT_BSP = $(K26SOM_BSP) 
 CURRENT_BSP_URL = $(KR260_BSP_URL) 
endif

ifeq ($(BOARD), kr260)
  CURRENT_BSP = $(KR260_BSP) 
  CURRENT_BSP_URL = $(KR260_BSP_URL) 
endif
    
ifeq ($(BOARD), daphnetestboard)
  CURRENT_BSP = $(K26SOM_BSP) 
  CURRENT_BSP_URL = $(KR260_BSP_URL) 
endif
    
    
.PRECIOUS: tmp/%.xpr tmp/%.xsa tmp/%.bit tmp/%.fsbl/executable.elf tmp/%.tree/system-user.dtsi petalinux devicetree.dtb dtbo wic wic2

xpr: tmp/$(NAME).xpr

xsa: tmp/$(NAME).xsa

all: tmp/$(NAME).bit tmp/$(NAME).xsa petalinux devicetree.dtb uimage

bit: tmp/$(NAME).bit

dtb: devicetree.dtb dtbo

petalinux: petalinuxprj petalinuxconfig petalinuxbuild 

uimage : sdimg wic

#$(KR260_BSP):
#	mkdir -p $(@D)
#	curl -L $(UBOOT_URL) -o $@

#$(SOM260_BSP):
#	mkdir -p $(@D)
#	curl -L $(UBOOT_URL) -o $@
	
tmp/%.xpr: projects/% $(addprefix tmp/cores/, $(CORES))
	mkdir -p $(@D)
	cp -R projects/$(NAME)/* tmp/
	$(VIVADO) -source scripts/project.tcl -tclargs $* $(PART)
	
tmp/%.xsa: tmp/%.bit
	mkdir -p $(@D)	
	$(VIVADO) -source scripts/hwXSA.tcl -tclargs $*

tmp/%.bit: tmp/%.xpr
	mkdir -p $(@D)
	$(VIVADO) -source scripts/bitstream.tcl -tclargs $*

$(DTREE_TAR):
	mkdir -p $(@D)
	curl -L $(DTREE_URL) -o $@
	
$(DTREE_DIR): $(DTREE_TAR)
	mkdir -p $@
	tar -zxf $< --strip-components=1 --directory=$@
	
dtbo:$(DT_PATH)/pl.dtsi
	dtc -@ -O dtb -o $(DT_PATH)/pl.dtbo $(DT_PATH)/pl.dtsi
	mkdir -p $(ABS_BUILD_PATH)/dt/pl_files_export 
	cp $(DT_PATH)/pl.dtbo tmp/dt/pl_files_export/
	cp $(ABS_BUILD_PATH)/$(NAME).bit $(ABS_BUILD_PATH)/dt/pl_files_export/$(NAME).bit.bin	
	echo '{ "shell_type" : "XRT_FLAT", "num_slots": "1" }' > $(ABS_BUILD_PATH)/dt/pl_files_export/shell.json
	@echo '[INFO]  Device tree files updated'
		
devicetree.dtb:
	-$(XSCT) $(ROOT_DIR)/scripts/devicetree.tcl $(NAME) $(BOARD) $(DTREE_TAG)

update_dtsi:
	@cp -f patches/system-user.dtsi $(PETALINUX_DIR)/project-spec/decoupling-dtsi/

update_config:
	# PetaLinux Config: Use EXT4 as rootfs format
	@echo '' >> $(PETALINUX_CONFIG)
	@echo '# ' >> $(PETALINUX_CONFIG)
	@echo '# PetaLinux user settings ' >> $(PETALINUX_CONFIG)
	@echo '# ' >> $(PETALINUX_CONFIG)
	@echo 'CONFIG_SUBSYSTEM_ROOTFS_EXT4=y' >> $(PETALINUX_CONFIG)
	#@echo 'CONFIG_SUBSYSTEM_DTB_OVERLAY=y' >> $(PETALINUX_CONFIG)
	@echo 'CONFIG_SUBSYSTEM_FPGA_MANAGER=y' >> $(PETALINUX_CONFIG)
	@echo '[INFO]  Petalinux Config file updated'

update_rootfs_config:
	@echo '' >> $(PETALINUX_ROOTFS_CONFIG)
	@echo '# ' >> $(PETALINUX_ROOTFS_CONFIG)
	@echo '# PetaLinux user settings ' >> $(PETALINUX_ROOTFS_CONFIG)
	@echo '# ' >> $(PETALINUX_ROOTFS_CONFIG)
	# PetaLinux Rootfs: Enable XRT
	@echo 'CONFIG_xrt=y' >> $(PETALINUX_ROOTFS_CONFIG)
	# echo 'CONFIG_xrt-dev=y' >> $(PETALINUX_ROOTFS_CONFIG)
	# PetaLinux Rootfs: Enable DNF
	@echo 'CONFIG_dnf=y' >> $(PETALINUX_ROOTFS_CONFIG)
	@echo 'CONFIG_imagefeature-package-management=y' >> $(PETALINUX_ROOTFS_CONFIG)
	# PetaLinux Rootfs: Enable packages for EoU
	@echo 'CONFIG_e2fsprogs-resize2fs=y' >> $(PETALINUX_ROOTFS_CONFIG)
	@echo 'CONFIG_parted=y' >> $(PETALINUX_ROOTFS_CONFIG)
	@echo 'CONFIG_resize-part=y' >> $(PETALINUX_ROOTFS_CONFIG)
	# PetaLinux Rootfs: Enable Vitis AI demo dependencies
	@echo 'CONFIG_packagegroup-petalinux-opencv=y' >> $(PETALINUX_ROOTFS_CONFIG)
	@echo 'CONFIG_mesa-megadriver=y' >> $(PETALINUX_ROOTFS_CONFIG)
	@echo 'CONFIG_packagegroup-petalinux-x11=y' >> $(PETALINUX_ROOTFS_CONFIG)
	@echo 'CONFIG_packagegroup-petalinux-v4lutils=y' >> $(PETALINUX_ROOTFS_CONFIG)
	@echo 'CONFIG_packagegroup-petalinux-matchbox=y' >> $(PETALINUX_ROOTFS_CONFIG)
	#Enable SSH and SCP connection
	@echo 'CONFIG_imagefeature-debug-tweaks=y' >> $(PETALINUX_ROOTFS_CONFIG)
	#@echo 'CONFIG_auto-login=y' >> $(PETALINUX_ROOTFS_CONFIG)		
	@echo '[INFO]  Petalinux Roots_config file updated'
	
petalinuxprj: 
	@cp $(ABS_BUILD_PATH)/$(NAME).runs/impl_1/*.bin $(ABS_BUILD_PATH)/$(NAME).bin 
	@cp $(ABS_BUILD_PATH)/$(NAME).runs/impl_1/*.bit $(ABS_BUILD_PATH)/$(NAME).bit
	@mkdir -p $(ABS_BUILD_PATH);
	@cd $(ABS_BUILD_PATH) && petalinux-create --force --type project -s $(ABS_BUILD_PATH)/$(CURRENT_BSP) --name $(PETALINUX_PROJ_NAME)	
	@echo '[INFO]  Petalinux project created'

patchfiles:
	@echo '[INFO]  Modify files to add custom configuration for devicetree'
	@echo 'INFO: Add and Modify user custom files'
	@echo 'INFO: URL: https://docs.xilinx.com/r/en-US/ug1144-petalinux-tools-reference-guide/Device-Tree-Configuration'
	cp $(ROOT_DIR)/patches/device-tree.bbappend $(PETALINUX_DIR)/project-spec/meta-user/recipes-bsp/device-tree/	
	cp $(ROOT_DIR)/patches/system-user.dtsi $(PETALINUX_DIR)/project-spec/meta-user/recipes-bsp/device-tree/files/
	cp $(ROOT_DIR)/patches/system-user-1.dtsi $(PETALINUX_DIR)/project-spec/meta-user/recipes-bsp/device-tree/files/
	@echo '[INFO]  Modifying the user pl-custom file'
	@echo 'INFO: These dtsi changes will take effect when the Device tree overlay is enabled in petalinux-config.'
	@echo 'INFO: URL: https://docs.xilinx.com/r/en-US/ug1144-petalinux-tools-reference-guide/DTG-Settings'	
	cp $(ROOT_DIR)/patches/pl-custom.dtsi $(PETALINUX_DIR)/project-spec/meta-user/recipes-bsp/device-tree/files/
	@echo 'INFO: Kernel configuration files'
	cp -f $(ROOT_DIR)/patches/linux-xlnx/user_daphnekernel.cfg $(PETALINUX_DIR)/project-spec/meta-user/recipes-kernel/linux/linux-xlnx/
	cp -f $(ROOT_DIR)/patches/linux-xlnx_%.bbappend $(PETALINUX_DIR)/project-spec/meta-user/recipes-kernel/linux/       
       	
petalinuxconfig:
	@cd $(PETALINUX_DIR) && petalinux-config --get-hw-description=$(ABS_BUILD_PATH)/$(NAME).xsa --silentconfig	
	$(MAKE)  update_config
	$(MAKE)  update_rootfs_config
	$(MAKE)  patchfiles
	@echo '[INFO]  Petalinux config updated'
	
petalinuxbuild:
	@cd $(PETALINUX_DIR) && petalinux-build		
	@echo '[INFO]  Petalinux Image file created'
	mkdir -p $(ABS_BUILD_PATH)/dt-linux/pl_files_export 
	cp $(ABS_BUILD_PATH)/$(PETALINUX_PROJ_NAME)/images/linux/pl.dtbo tmp/dt-linux/pl_files_export/
	cp $(ABS_BUILD_PATH)/$(NAME).bit $(ABS_BUILD_PATH)/dt-linux/pl_files_export/$(NAME).bit.bin	
	echo '{ "shell_type" : "XRT_FLAT", "num_slots": "1" }' > $(ABS_BUILD_PATH)/dt-linux/pl_files_export/shell.json
	@echo '[INFO]  Device tree overlay files updated'
	
wic2:
	@cd $(PETALINUX_DIR) && petalinux-package --boot --u-boot --force
	rm -f $(PETALINUX_DIR)/pre-built/linux/images/system.bit
	cp $(PETALINUX_DIR)/images/linux/design_1_wrapper.bit $(PETALINUX_DIR)/pre-built/linux/images/system.bit 
	cp -r $(PETALINUX_DIR)/images/linux/* $(PETALINUX_DIR)/pre-built/linux/images/
	@cd $(PETALINUX_DIR) && petalinux-package --wic --images-dir images/linux --bootfiles "ramdisk.cpio.gz.u-boot, boot.scr, Image, system*.dtb, system-zynqmp-sck-kr-g-revB.dtb" --disk-name "sda"
	#mv $(PETALINUX_DIR)/images/linux/petalinux-sdimage.wic $(SD_CARD)/wic/
	@echo '[INFO]  Petalinux - wic2 file created'
		
    			
#Info: https://docs.xilinx.com/r/en-US/ug1144-petalinux-tools-reference-guide/Steps-to-Boot-a-PetaLinux-Image-on-Hardware-with-SD-Card
sdimg:
	mkdir -p $(SD_CARD)/img	
	cp $(IMAGE_DIR)/BOOT.BIN $(SD_CARD)/img
	cp $(IMAGE_DIR)/boot.scr $(SD_CARD)/img
	cp $(IMAGE_DIR)/system.dtb $(SD_CARD)/img
	cp $(IMAGE_DIR)/Image $(SD_CARD)/img
	#cp run_app.sh $(SD_CARD)
	$(XILINX_VITIS)/scripts/vitis/util/mkfsImage.sh -s $(SD_CARD)/img -o $(SD_CARD)/img/sd_card.img -m 1 -e $(PETALINUX_DIR)/images/linux/rootfs.ext4
	@echo '[INFO]  Petalinux Image file created'

#Info: Wic image default size 2G + 4G = 6G 
wic: 
	mkdir -p $(SD_CARD)/wic	
	cp $(PETALINUX_DIR)/pre-built/linux/images/ramdisk.cpio.gz.u-boot $(PETALINUX_DIR)/images/linux
	cd $(PETALINUX_DIR) && petalinux-package --wic --images-dir images/linux --bootfiles "ramdisk.cpio.gz.u-boot, boot.scr, Image, system*.dtb, system-zynqmp-sck-kr-g-revB.dtb" --disk-name "sda"
	mv $(PETALINUX_DIR)/images/linux/petalinux-sdimage.wic $(SD_CARD)/wic/
	@echo '[INFO]  Petalinux - wic file created'
	
#You can also rebuild some of the images alone as follows:
fsbl:
	cd $(PETALINUX_DIR) && petalinux-build -c bootloader

u-boot:
	cd $(PETALINUX_DIR) && petalinux-build -c u-boot

rebuild_dtb:
	cd $(PETALINUX_DIR) && petalinux-build -c device-tree
	mkdir -p $(ABS_BUILD_PATH)/dt-linux/pl_files_export 
	cp $(ABS_BUILD_PATH)/$(PETALINUX_PROJ_NAME)/images/linux/pl.dtbo tmp/dt-linux/pl_files_export/
	cp $(ABS_BUILD_PATH)/$(NAME).bit $(ABS_BUILD_PATH)/dt-linux/pl_files_export/$(NAME).bit.bin	
	echo '{ "shell_type" : "XRT_FLAT", "num_slots": "1" }' > $(ABS_BUILD_PATH)/dt-linux/pl_files_export/shell.json
	@echo '[INFO]  Device tree overlay files updated'

kernel:
	cd $(PETALINUX_DIR) && petalinux-build -c kernel
	
clean:
	$(RM) tmp/$(NAME)* tmp/*.bin tmp/dt/dt tmp/dt/dt tmp/dt/pl_files_export 
	$(RM) tmp/sdcard/ tmp/psu*
