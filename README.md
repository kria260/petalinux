# Petalinux
The project must be into /projects Folder

## make only XSA project
```
make NAME=test xsa
```

## make Petalinux -> project create, config and build
```
make NAME=test petalinux
```
### Petalinux -> Project create
```
make NAME=test petalinuxprj
```
### Petalinux -> config
```
make NAME=test petalinuxconfig
```
### Petalinux -> build
```
make NAME=test petalinuxbuild
```
### Petalinux device tree(Using Petalinux)
To add a new Petalinux configuration, it must be added in update_rootfs_config and update_config inside the makefile and then do a rebuild_dtb or petalinuxbuild. 
```
make NAME=test rebuild_dtb
```
### make device tree 
This is created outside of the Petalinux folder
```
make NAME=test dtb
```
### create WIC image
This create an Petalinux-sdimage.wic file thats contains two partitions: boot(2G) and root(4G) 
```
make NAME=test wic
```
### create an IMG image
This create an sd_card.img file thats contains two partitions: boot(1G) and root(2G) 
```
make NAME=test sdimg
```
### Build the whole project until the *.wic and *.img image is created

```
make NAME=test all
```
