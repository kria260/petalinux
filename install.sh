#!/bin/bash
PETALINUX = 2022.2
PETALINUX_VERSION = v2022.2-10141622

echo 'vivado requirements'

sudo apt-get install libtinfo5
sudo apt install libncurses5

if [ -f "$newfile1" ]
then
echo "File is found"
else
   echo "File is not found"
fi

echo 'petalinux requirements'
sudo apt install build-essential
sudo apt-get install manpages-dev
gcc --version

sudo apt-get install xterm autoconf libtool texinfo gcc-multilib zlib1g zlib1g-dev zlib1g:i386

sudo apt-get install pax iproute2 net-tools libncurses5-dev libssl-dev flex bison libselinux1\
screen python3 python3-pexpect python3-pip python3-git python3-jinja2 xz-utils debianutils \
iputils-ping libegl1-mesa libsdl1.2-dev pylint3 cpio
dpkg-reconfigure -p critical dash

sudo mkdir -p /opt/pkg/petalinux/$PETALINUX/
sudo chown $USER:$USER /opt/pkg/petalinux/$PETALINUX

chmod 755 ./petalinux-v<petalinux-version>-final-installer.run
./petalinux-$PETALINUX_VERSION-installer.run --dir /opt/pkg/petalinux/2022.2/

#Adicional libraries
sudo apt-get install make tftpd wget diffstat chrpath socat tar unzip gzip tofrodos
sudo apt-get install pylint python2 tftpd gnupg haveged perl
sudo apt-get install lib32stdc++6 libgtk2.0-0:i386 libfontconfig1:i386 libx11-6:i386 libxext6:i386 libxrender1:i386 libsm6:i386 
sudo apt-get install xinetd gawk gcc ncurses-dev openssl 
sudo apt-get install build-essential automake putty g++
sudo apt-get install liberror-perl mtd-utils xtrans-dev libxcb-randr0-dev libxcb-xtest0-dev libxcb-xinerama0-dev libxcb-shape0-dev libxcb-xkb-dev
sudo apt-get install openssh-server util-linux sysvinit-utils google-perftools
sudo apt-get install libncurses5 libncursesw5-dev libncurses5:i386 libtinfo5
sudo apt-get install libstdc++6:i386 libgtk2.0-0:i386 dpkg-dev:i386
sudo apt-get install ocl-icd-libopencl1 opencl-headers ocl-icd-opencl-dev
sudo apt-get install tofrodos gawk xvfb git tftpd \
libssl-dev chrpath socat autoconf libglib2.0-dev 

sudo echo ' service tftp
    {
    protocol = udp
    port = 69
    socket_type = dgram
    wait = yes
    user = nobody
    server = /usr/sbin/in.tftpd
    server_args = /tftpboot
    disable = no
    }' >> /etc/xinetd.d/tftp

sudo mkdir /tftpboot
sudo chmod -R 777 /tftpboot
sudo chown -R nobody /tftpboot

sudo /etc/init.d/xinetd stop
sudo /etc/init.d/xinetd start

